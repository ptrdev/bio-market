/**
 * Created by PT on 8/25/2017.
 */
window.custom = window.custom || {};
var lat;
var lng;
(function ($, custom, enquire) {

    'use strict';

    custom = custom || {};

    custom.banner = function () {

        var self = this;
        var delay = 3000;
        self.slide_index = 1;
        self.pause = false;

        self.initialize = function () {

            self.startSubmitListener();


        };


        self.startSubmitListener = function () {

            self.displaySlides(self.slide_index);
            //self.loop();
            setTimeout(self.loop, 6000);

        };

        self.loop = function () {
            if (!self.pause) {

                self.nextSlide(self.slide_index + 1);
                setTimeout(self.loop, 6000);
            } else {
                setTimeout(self.loop, 3000);
            }


        };


        self.nextSlide = function (n) {
            self.slide_index = n;
            self.displaySlides(self.slide_index);
        };
        self.currentSlide = function (n) {
            self.displaySlides(self.slide_index = n);
        };
        self.getIndex = function () {
            return self.slide_index;
        };
        self.displaySlides = function (n) {
            var i;
            var slides = document.getElementsByClassName("slide");
            if (n > slides.length) {
                self.slide_index = 1
            }
            if (n < 1) {
                self.slide_index = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            slides[self.slide_index - 1].style.display = "block";
        };

        self.initialize();

    };

    custom.imageRotator = function (form) {
        var self = this;
        var images = form.find('.product-image');
        var actual = 0;
        self.initialize = function () {
            self.startListener();
        };

        self.startListener = function () {
            images[actual].className += ' visible';
        };

        self.change = function (index) {
            /* var remove = images[actual].className;
             var re = new RegExp('(^| )' + ' visible' + '( |$)');
             remove = remove.replace(re, '$1');
             remove = remove.replace(/ $/, '');*/
            // images[actual].className.replace(/\bvisible\b/,'');
            images[actual].classList.remove("visible");

            actual = index;
            images[index].className += ' visible';


        };

        self.initialize();
    };


$(document).ready(function () {

    /* Chrome need SSL! */
    var is_chrome = /chrom(e|ium)/.test( navigator.userAgent.toLowerCase() );
    var is_ssl    = 'https:' == document.location.protocol;
    if( is_chrome && ! is_ssl ){

    }

    /* HTML5 Geolocation */
    navigator.geolocation.getCurrentPosition(
        function( position ){ // success cb

            /* Current Coordinate */
            lat = position.coords.latitude;
            lng = position.coords.longitude;
            var google_map_pos = new google.maps.LatLng( lat, lng );

            /* Use Geocoder to get address */
            var google_maps_geocoder = new google.maps.Geocoder();
            google_maps_geocoder.geocode(
                { 'latLng': google_map_pos },
                function( results, status ) {
                    if ( status == google.maps.GeocoderStatus.OK && results[0] ) {
                        console.log( results[0].formatted_address );
                    }
                }
            );
        });

    $('#postOrder').submit(function () {
        var data = $(this).serialize() + "&customer_id=" + logged.id + "&product_id=" + prod_id;
        $.ajax({
            url: '/makeOrder',
            type: 'post',
            dataType: 'json',
            data: data

        }).done(function (response) {
            $('#postOrder')[0].reset();
        });
        return false;
    });

    $('#postMessage').submit(function () {
        var seller_id = $('#seller_id').value;
        var data = $(this).serialize() + "&customer_id=" + logged.id + "&product_id=" + seller_id;
        $.ajax({
            url: '/sendMessage',
            type: 'post',
            dataType: 'json',
            data: data

        }).done(function (response) {
            $('#postMessage')[0].reset();
        });
        return false;
    });

    if ($('#main-banner').length) {
        var left = $(".left");
        var right = $(".right");
        var banner = new custom.banner($('#main-banner'));
        left.click( function () {
            banner.nextSlide(banner.getIndex()-1);
        });
        right.click( function () {
            banner.nextSlide(banner.getIndex()+1);
        });
        $('#main-banner').mouseenter(function () {
            event.preventDefault();
            banner.pause = true;
        });
        $('#main-banner').mouseleave(function () {
            event.preventDefault();
            banner.pause = false;
            //setTimeout(banner.loop, 6000);
        });
    }

    if ($('#vege').length ) {
        var imageRotator = new custom.imageRotator($('#vege'));
        $('#vege').find('li').hover(
            function () {
                //console.log($(this).attr('id'));
                imageRotator.change($(this).attr('value'));


            }
        );
    }

    if ($('#gluten').length) {
        var imageRotator2 = new custom.imageRotator($('#gluten'));
        $('#gluten').find('li').hover(
            function () {
                //console.log($(this).attr('id'));
                imageRotator2.change($(this).attr('value'));


            }
        );
    }

    if ($('#meat').length) {
        var imageRotator3 = new custom.imageRotator($('#meat'));
        $('#meat').find('li').hover(
            function () {
                //console.log($(this).attr('id'));
                imageRotator3.change($(this).attr('value'));


            }
        );
    }

    if ($('#login').length) {

       // var loginForm = new custom.loginForm();
        $('#login').click(
            function () {

                $('#loginModal').fadeIn(600);
            }
        );

        $('.modal .close').click(function () {
            $('.modal').fadeOut(500);
        });

        $('.modal').click(function (event) {
           if ($('.modal').is(event.target)) {
                $('.modal').fadeOut(500);
            }
        });

    }

    if (typeof logged !== "undefined") {
    if ($('#order').length) {

        // var loginForm = new custom.loginForm();
        $('#order').click(
            function () {

                $('#orderModal').fadeIn(600);
            }
        );

        $('.modal .close').click(function () {
            $('.modal').fadeOut(500);
        });

        $('.modal').click(function (event) {
            if ($('.modal').is(event.target)) {
                $('.modal').fadeOut(500);
            }
        });

    } }
    else {
        if ($('#order').length) {

            $('#order').css('background', '#eee');

        }
    }

    if ($('#login2').length) {
        $('#login2').click(
            function () {
                $('#loginModal').fadeIn(600);
            }
        );

        $('.modal .close').click(function () {
            $('.modal').fadeOut(500);
        });

        $('.modal').click(function (event) {
            if ($('.modal').is(event.target)) {
                $('.modal').fadeOut(500);
            }
        });


    }
    if ($('#msg').length) {
        $('div#msg.btn-success').click(
            function () {
                console.log('kokotskurvenypojebany');
                $('#messageModal').fadeIn(600);
            }
        );
        $('.modal .close').click(function () {
            $('.modal').fadeOut(500);
        });
        $('.modal').click(function (event) {
            if ($('.modal').is(event.target)) {
                $('.modal').fadeOut(500);
            }
        });
    }

    if ($('.message').length) {
        // var loginForm = new custom.loginForm();
        $('.message').click(
            function () {

                $('.messageModal').addClass('show');
            }

        );
        $('body').click(function(event){
            if (! event.target.matches('.messageModal') && ! event.target.matches('.icon-bubbles3')) {
                $('.messageModal').removeClass('show');
            }
        });



    }

    if ($('#register').length) {
        // var loginForm = new custom.loginForm();
        $('#register').click(
            function () {
                $('.loader').show();
                var data = $('.form-horizontal').serialize();
                console.log(data);

                $.ajax({
                    url: '/registerUser',
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function () {


                    }
                }).done(function () {


                });
                return false;

            }

        );

    }

    if ($('#menu-btn').length) {
        var bool = false;
        // var loginForm = new custom.loginForm();
        $('#menu-btn').click(
            function () {
                if (bool == false) {
                    $('.hammenu').addClass('menu-active');
                    bool = true;
                    $('#menuModal').animate({
                        left: "+=60%"
                    }, 500, function () {


                    });
                } else {
                    $('.hammenu').removeClass('menu-active');
                    bool = false;
                    $('#menuModal').animate({
                        left: "-60%"
                    }, 500, function () {


                    });
                }

            });




        $('.side-menu').find('a').click(function () {
            bool = false;
            $('.hammenu').removeClass('menu-active');
            $('#menuModal').animate({
                left: "-60%"
            }, 500, function () {

            });
        });



    }


});


    })(jQuery, window.custom, window.enquire);

function nearestPlaces(){
    window.location.href="https://bio-market.sk/near/"+lat+"/"+lng;

}




