/**
 * Created by PT on 9/12/2017.
 */
$(document).ready(function () {
    // Hide Header on on scroll down
    var previousScroll = 0;
    $('main').css('margin-top', $('header').height()    );
    $(window).scroll(function () {
        var currentScroll = $(this).scrollTop();

            if (currentScroll > previousScroll && !$('.hammenu').hasClass('menu-active') && !$('.messageModal').hasClass('show')) {
                $('header').removeClass('show').addClass('hide');
            }
            else {
                $('header').removeClass('hide').addClass('show');
            }
            previousScroll = currentScroll;

    });
    });