window.custom = window.custom || {};

(function ($, custom, enquire) {

    'use strict';

    custom = custom || {};

    $(document).ready(function () {

        $(document).delegate('.delete-category','click',function () {
            $('.loader').show();
            var cat_id = this.id;
            var category = $('#' + cat_id);
            $.ajax({
                url: '/cpanel/deleteCategory/' + cat_id,
                type: 'get',
                dataType: 'json',
                data: $(this).serialize(),
                success: function () {
                    console.log('done');
                    $('.loader').hide();
                }
            }).done(function () {
                console.log('done');

                category.remove();

            });
            return false;
        });
    });


})(jQuery, window.custom, window.enquire);