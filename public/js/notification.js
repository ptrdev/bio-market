/**
 * Created by PT on 2/16/2018.
 */
var socket = io('https://bio-market.sk:3000', {secure: false});
jingle = new Audio(window.location.protocol+'//'+window.location.host+'/public/knocking.wav');

socket.on("test-channel-"+logged.id+":App\\Events\\TestEvent", function(message){
    if ($('#new-message').length != 0) {
        $('#new-message').html(message.data.message);
    } else {
        $('#msg-btn').prepend('<span id="new-message" class="label label-danger">'+message.data.message+'</span>');
    }
    jingle.play();
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    }

    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        var notification = new Notification("Hi there!");
    }

    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== "denied") {
        Notification.requestPermission(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                var notification = new Notification("Hi there!");
            }
        });
    }
});