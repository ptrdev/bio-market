/**
 * Created by PT on 9/18/2017.
 */
$(document).ready(function () {

        $(document).delegate('.delete','click',function () {
            $('.loader').show();
            var product_id = this.id;
            var product = $('#product' + product_id);
            $.ajax({
                url: '/dashboard/delete/' + product_id,
                type: 'get',
                dataType: 'json',
                data: $(this).serialize(),
                success: function () {
                    console.log('done');
                    $('.loader').hide();
                }
            }).done(function () {
                console.log('done');

                product.remove();

            });
            return false;
        });

        $('.accept-order').click(
            function () {
                var id = $(this).find($("input"))[0].value;
                var status = 2;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/dashboard/update_order_status/' + id +'/' + status,
                    type: 'patch',
                    dataType: 'json',
                    success: function (response) {
                        $("#status_"+id).html("");
                        $("#status_"+id).html("<div class='success'>Schválená</div>");
                        console.log(response);
                    }
                }).done(function () {

                });
                return false;
            }
        );

        $('.dismiss-order').click(
            function () {
                var id = $(this).find($("input"))[0].value;
                var status = 1;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/dashboard/update_order_status/' + id +'/' + status,
                    type: 'patch',
                    dataType: 'json',
                    success: function (response) {
                        $("#status_"+id).html("");
                        $("#status_"+id).html("<div class='error'>Zamietnutá</div>");
                        console.log(response);
                    }
                }).done(function () {

                });
                return false;
            }
        );

        $('.update').click(

            function () {
                $('.loader').show();
                var product_id = $('.edit-prod').attr('id');
                var x = $('.edit-prod #prod-img').attr('src');
                var img = '';
                if(x.substring(0,8) == '../images') {
                    img = x.replace('../images/','');
                } else {
                    img = x.replace('images/','');
                }

                img = img.replace('.','_');
                img = img.replace('/','-');
                img = img.replace('/','-');
                var data = $('.edit-prod').serialize() + '&img=' + img;
                console.log($('.edit-prod #name').val());
                $.ajax({
                    url: '/dashboard/updateProduct/' + product_id,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    success: function (product) {
                        console.log('done');
                       // var pro = product.product;
                       // console.log(pro.id);
                        $('#product'+ product_id + ' .name').text($('.edit-prod #name').val());
                        $('#product'+ product_id + ' img').attr('src',x);
                        $('.loader').hide();
                      //  $("form.add")[0].reset();
                      //  $('#prod-img').attr('src','');
                    }
                }).done(function () {
                    console.log('done');
                    $('.editProduct').animate({
                        left: "-50%"
                    }, 500, function () {
                    });
                });
                return false;
            });



        $('.edit-prod input[type=file]').on('change', function(event){
            //var formData = new FormData();
           // formData.append('file', $(this)[0].files[0]);
           // var filevalue = $('file').val();
            $('.small-loader').show();
            var product_id = $('.usr').attr('id');
            var files;
            files = event.target.files;
            var data = new FormData();
            var x = $('.edit-prod #prod-img').attr('src');
            console.log(x);
            $.each(files, function(key, value)
            {
                data.append(key, value);
            });
            if (x != '') {
                console.log(x);
                data.append('img',x);
            }
            console.log(data);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fileUpload/' + product_id,
                type: 'post',
                data: data,
                async: true,
                cache: false,
                dataType: 'json',
                contentType : false,
                processData: false,
                success: function (message) {
                    console.log('done');
                    console.log(message);
                   // $('#image').val('images/'+message.message);
                    $('.edit-prod #prod-img').attr('src',message.message);
                    $('.small-loader').hide();
                }
            }).done(function () {
                console.log('done');

            });
            return false;
        });

        $('input[type=file]').on('change', function(event){
            //var formData = new FormData();
            // formData.append('file', $(this)[0].files[0]);
            // var filevalue = $('file').val();
            $('.small-loader').show();
            var product_id = $('.add').attr('id');
            var files;
            files = event.target.files;
            var data = new FormData();
            var x = $('#prod-img').attr('src');
            console.log(x);
            $.each(files, function(key, value)
            {
                data.append(key, value);
            });
            if (x != '') {
                console.log(x);
                data.append('img',x);
            }
            console.log(data);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/fileUpload/' + product_id,
                type: 'post',
                data: data,
                async: true,
                cache: false,
                dataType: 'json',
                contentType : false,
                processData: false,
                success: function (message) {
                    console.log('done');
                    console.log(message);
                    // $('#image').val('images/'+message.message);
                    $('#prod-img').attr('src',message.message);
                    $('.small-loader').hide();
                }
            }).done(function () {
                console.log('done');

            });
            return false;
        });
});
