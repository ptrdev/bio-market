<?php

return [

    /*
     * Set the default API url
     */
    'api-url' => 'http://api.ipinfodb.com',

    /*
     * Set the default API version
     */
    'api-version' => 'v3',

    /*
     * City Precision url
     */
    'api-city' => 'ip-city',

    /*
     * Country Precision url
     */
    'api-country' => 'ip-country',

    /*
     * Set the auth ID from the .env file, or set it here.
     */
    'api-key' => env('GEOLOCATION_API_KEY', '39c2448fb9758d5328da056c7547236eb1306af00815adbb4c1d2e9ace2a7af5'),

    /*
       * How long should the request response be cached for (mins)
       */
    'cache-duration' => env('GEOLOCATION_CACHE', '5'),


];

