/**
 * Created by PT on 1/24/2018.
 */
var app = require('express')();
var cors = require('cors');


var fs = require('fs');
var https = require('https');

var options = {
    key: fs.readFileSync('./file.pem'),
    cert: fs.readFileSync('./file.crt'),
    requestCert: false,
    rejectUnauthorized: false
};


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('*',function(req,res){
    res.redirect('https://bio-market.sk:3000'+req.url)
});

var server = https.createServer(options, app);
var io = require('socket.io')(server);
//io.origins('*:*');
io.on('connection', function(socket) {
    console.log("connected");
});
var Redis = require('ioredis');
var redis = new Redis();

redis.psubscribe('*', function(err, count) {
});
redis.on('pmessage', function(subscribed, channel, message) {
    message = JSON.parse(message);
    console.log(message);
    console.log(channel);
    io.emit(channel + ':' + message.event, message.data);
});
server.listen(3000, function(){
    console.log('Listening on Port 3000');
});

redis.on("error", function (err) {
    console.log(err);
});