<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\Auth\Guard;

class CachePage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = $request->fullUrl();

        if (Cache::has($key))
            return response(Cache::get($key));

        $response = $next($request);

        $cachingTime = 60;
        Cache::put($key, $response->getContent(), $cachingTime);

        return $response;
    }
}
