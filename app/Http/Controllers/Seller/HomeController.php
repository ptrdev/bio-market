<?php

namespace App\Http\Controllers\Seller;

use App\Product;
use App\Category;
use App\User;
use App\Village;
use Auth;
use DB;
use Image;
use File;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Midnite81\GeoLocation\Contracts\Services\GeoLocation;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $id = Auth::user()->id;
        return view('dashboard.index',compact('id'));
    }

    public function productsAll()
    {
        $id = Auth::user()->id;
        $categories = Category::all();
        $products = DB::table('products')->where('user_id', $id)->get();
        if (Auth::user()->hasRole('seller')) {
            return view('dashboard.products',compact('products','id','categories'));
        }

    }


    public function addProduct($id)
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        $categories = Category::all();
        return view('dashboard.add_product', compact('user','categories','id'));
    }


    public function edit($id){
        $product = Product::find($id);
        $categories = Category::all();
        $id = Auth::user()->id;
        return view('dashboard.edit',compact('product','categories','id'));
    }

    public function editProduct($id){
        $product = Product::find($id);
        $categories = Category::all();
        $id = Auth::user()->id;
        return view('dashboard.edit',compact('product','categories','id'));
        /*$product = Product::find($id);
        $categories = Category::all();

        return response()->json(['success' => 'true', 'product' => $product, 'categories' => $categories]);*/
    }

    public function delete(Product $id) {

    $id->delete();
        return response()->json(['success' => 'true']);
    }

    public function destroy( $id) {
        $product =Product::find($id);
        $product->delete();
    }


    public function updateProduct(Request $request, $id) {

        $product = Product::find($id);

        $product->name = $request->input('name', false);
        $product->image = $request['image'];
        $product->price = $request->input('price', false);
        $product->unit = $request->input('unit', false);
        $product->discount = $request->input('discount', false);
        $product->description = $request->input('description', false);
        $product->category_id = $request->input('category',false);


        $product->save();

        return $this->index();
    }

    public function updateUserInfo($id) {
        $user = User::find($id);
        $id = Auth::user()->id;
        $villages = Village::all();
        return view('dashboard.edit_user',compact('user','id','villages'));
    }

    public function updateUser(Request $request, $id) {

        $user = User::find($id);
        $last = User::find($id);

        $user->name = $request->input('name', false);
        $user->email = $request->input('email', false);
        $user->description = $request->input('description', false);
        $user->village_id = $request->input('village', false);

        if ($request['avatar']) {
            $user->avatar = '..'.$request['avatar'];
        } else {
            $user->avatar = $last->avatar;
        }

        if ($request['banner']) {
            $user->banner = '..'.$request['banner'];
        } else {
            $user->banner = $last->banner;
        }
        $user->save();

        return $this->index();
    }


    public function createProduct(Request $request, $id)
    {
        $product = new Product();
        $id = Auth::user()->id;
        $product->name = Input::get('name');
        $product->price = Input::get('price');
        $product->unit = Input::get('unit');
        $product->description = Input::get('description');
        $product->short_description = Input::get('short_description');
        $product->discount = -1;
        $product->category_id = Input::get('category');
        if($request['time_from'] && $request['time_to']) {
            $product->daily_availability_from = $request['time_from'];
            $product->daily_availability_to = $request['time_to'];
        } else if ($request['date_from'] && $request['date_to']) {
            $product->availability_from = $request['date_from'];
            $product->availability_to = $request['date_to'];
        }
        $product->image = Input::get('image ');
        $product->user_id = $id;
        $product->save();

        return view('dashboard.index',compact('id'));
    }

    public function fileUpload(Request $request, $id)

    {

        $input = Input::all();
        $image = $input[0];


        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
        if ($request->img) {
            $img = $request->img;
            File::delete($img);
        }
        $destinationPath = public_path('images/product/'.$id);
        $thumb = Image::make($image->getRealPath())->resize(180,180, function ($constraint) {
            $constraint->aspectRatio();
        });
        $canvas = Image::canvas(180, 180);
        $canvas->insert($thumb, 'center');
        if(!is_dir($destinationPath)) {
            File::makeDirectory($destinationPath, 0777, true);
        }
        $canvas->save($destinationPath.'/'.$input['imagename'],80);

        //$image->move($destinationPath, $input['imagename']);
        return response()->json(['success' => 'true', 'message' => 'images/product/'.$id.'/'.$input['imagename']]);

    }

    public function myOrders() {
        $id = Auth::user()->id;
        $products = DB::table('products')->where('user_id', $id)->pluck('id');
        $data = collect($products)->map(function($x){ return (array) $x; })->toArray();
        $orders =  DB::table('orders')->whereIn('product_id', $products)->select('orders.*', 'users.name', 'users.email', 'products.name AS prod_name', 'products.unit')->join('products', 'orders.product_id', '=', 'products.id')->leftJoin('users', 'orders.customer_id', '=', 'users.id')->get();
        return view('dashboard.orders',compact('orders','id'));
    }

    public function orders() {
        $id = Auth::user()->id;
        $orders =  DB::table('orders')->where('customer_id', $id)->select('orders.*', 'users.name', 'users.email', 'products.name AS prod_name', 'products.unit')->join('products', 'orders.product_id', '=', 'products.id')->leftJoin('users', 'products.user_id', '=', 'users.id')->get();

        return view('dashboard.my_orders',compact('orders','id'));
    }

    public function updateOrderStatus($id, $status) {
        $order = Order::find($id);
        $order->status = $status;

        $order->save();

        return response()->json(['message' => 'Objednávka aktualizovaná', 'status' => $status]);
    }


}
