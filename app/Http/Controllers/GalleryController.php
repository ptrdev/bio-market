<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Image;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index($id)
    {
        $images = DB::table('images')->where('gallery_id',$id)->get();
        $id = Auth::user()->id;
        return view('dashboard.product_gallery',compact('images','id'));
    }


    public function upload(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $gallery = DB::table('galleries')->where('product_id', $id)->get();
        if (!$gallery->isEmpty()) {
            $gallery = new Gallery();
            $gallery->product_id = $id;
        }
        foreach ($request->image as $photo) {
            $image = new Image();
            $image->image = time().'.'.$photo->getClientOriginalExtension();
            $photo->move(public_path('images'), $photo);
            $image->gallery_id = $gallery->id;
            $image->save();
        }


        return back()
            ->with('success','Image Uploaded successfully.');
    }


    public function destroy($id, $image_id)
    {
        $image = DB::table('images')
        ->where([
                'gallery_id' => $id,
                'id' => $image_id
        ])->get();
        $image->delete();
        return back()
            ->with('success','Image removed successfully.');
    }
}
