<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use DB;
use App\User;
use Illuminate\Support\Facades\Redis;
use App\Events\TestEvent;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::find(Auth()->id());
        //$location =  $geo->getCity($request->ip());
        //dd($request);
        $products = DB::table('products')
            ->select('products.*', 'villages.fullname AS village')
            ->take(8)->orderBy('products.created_at','DESC')
            ->join('users', 'users.id', '=', 'products.user_id')
            ->leftjoin('villages', 'villages.id', '=', 'users.village_id')
            ->get();
        $favourite = DB::table('products')
            ->select('products.*', 'ratings.rate', 'villages.fullname AS village')
            ->join('ratings', 'ratings.product_id', '=', 'products.id')
            ->join('users', 'users.id', '=', 'products.user_id')
            ->leftjoin('villages', 'villages.id', '=', 'users.village_id')
            ->take(8)->orderBy('rate','DESC')->get();
        $categories = DB::table('categories')->whereNull('parent_id')->get();
        $subCategories = [];

        foreach ($categories as $parent) {
            $sub = DB::table('categories')->where('parent_id','=',$parent->id)->get();
            array_push($subCategories,$sub);
        }
        /*if ($user) {
            event(new TestEvent($user,'hello'));
        }*/
        //Redis::publish('test-channel', json_encode($event));
        return view('home',compact('products', 'categories','subCategories','favourite'));
    }
}
