<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use App\Comment;
use App\Category;
use DB;
use Illuminate\Support\Facades\Redis;
use App\Events\TestEvent;
use Illuminate\Http\Request;
use App\Order;

class PagesController extends Controller
{
    public function near($latitude,$longitude) {


        $responseStyle = 'short';
        $citySize = 'cities150';
        $radius = 18;
        $maxRows = 1000;

        $nearbyCities = json_decode(file_get_contents('http://api.geonames.org/findNearbyPlaceNameJSON?lat='.$latitude.'&lng='.$longitude.'&style='.$responseStyle.'&cities='.$citySize.'&radius='.$radius.'&maxRows='.$maxRows.'&username='."ptr_dev", true));
        $cities = [];
        foreach($nearbyCities->geonames as $cityDetails)
        {
            array_push($cities,$cityDetails->name);
        }
        $products = DB::table('products')
            ->select('products.*', 'villages.fullname AS village')
            ->take(50)->orderBy('created_at','DESC')
            ->join('users', 'users.id', '=', 'products.user_id')
            ->leftjoin('villages', 'villages.id', '=', 'users.village_id')
            ->whereIn('villages.fullname',$cities)
            ->paginate(4);
        $categories = DB::table('categories')->whereNull('parent_id')->get();
        $subCategories = [];
        foreach ($categories as $parent) {
            $sub = DB::table('categories')->where('parent_id','=',$parent->id)->get();
            array_push($subCategories,$sub);
        }
        $title = 'V okolí';
        return view('archive',compact('products','categories','subCategories','title'));
    }

    public function detail($id){
        $user = User::find(Auth()->id());
        $product = Product::find($id);
            if ($product->category_id) {
            $cats = Category::find($product->category_id);
            if ($cats->parent_id) {
                $parent = Category::find($cats->parent_id);
            } else {
                $parent = null;
            }
        } else {
            $cats = null;
            $parent = null;
        }
        $categories = DB::table('categories')->whereNull('parent_id')->get();
        $subCategories = [];
        foreach ($categories as $par) {
            $sub = DB::table('categories')->where('parent_id','=',$par->id)->get();
            array_push($subCategories,$sub);
        }
        $seller = User::find($product->user_id);
        $comments = Comment::all()->where('product_id','=',$id);
        $products = Product::all()->where('user_id','=',$product->user_id)->where('id','<>',$product->id);
        /*if ($user) {
            event(new TestEvent($user->id,'hello'));
        }*/
        return view('detail',compact('product', 'seller', 'products', 'comments', 'user', 'cats', 'parent','categories','subCategories'));
    }

    public function category($id){
        $category = Category::find($id);
        if (!$category->parent_id) {
            $children = $category->children;
        }
        if (!$category->parent_id) {
            $arr = [];
            foreach ($children as $child) {
                array_push($arr, $child->id);
            }
            array_push($arr, $id);
            $products = Product::whereIn('category_id', $arr)->get();
        } else {
            $products = Product::all()->where('category_id','=',$id);
        }
        $categories = DB::table('categories')->whereNull('parent_id')->get();
        $subCategories = [];
        foreach ($categories as $parent) {
            $sub = DB::table('categories')->where('parent_id','=',$parent->id)->get();
            array_push($subCategories,$sub);
        }
        return view('category',compact('products', 'category','categories','subCategories'));
    }

    public function sellerDetail($id){
        $seller = User::find($id);
        $products = Product::all()->where('user_id','=',$seller->id);
        $categories = DB::table('categories')->whereNull('parent_id')->get();
        $subCategories = [];
        foreach ($categories as $parent) {
            $sub = DB::table('categories')->where('parent_id','=',$parent->id)->get();
            array_push($subCategories,$sub);
        }
        return view('seller-detail',compact('seller', 'products','categories','subCategories'));
    }

    public function newest()
    {
        $products = DB::table('products')
            ->select('products.*', 'villages.fullname AS village')
            ->take(50)->orderBy('created_at','DESC')
            ->join('users', 'users.id', '=', 'products.user_id')
            ->leftjoin('villages', 'villages.id', '=', 'users.village_id')
            ->paginate(4);
        $categories = DB::table('categories')->whereNull('parent_id')->get();
        $subCategories = [];
        foreach ($categories as $parent) {
            $sub = DB::table('categories')->where('parent_id','=',$parent->id)->get();
            array_push($subCategories,$sub);
        }
        $title = 'Najnovšie';
        return view('archive',compact('products','categories','subCategories','title'));
    }

    public function storeComment(Request $request)
    {
       /* $this->validate($request, [
            'comment' => 'required',
            'reply_id' => 'filled',
            'product_id' => 'filled',
            'user_id' => 'required',
        ]);*/
        $comment = new Comment();
        $data = $request->all();
    $p_id = $data['product_id'];
        $com = $data['comment'];
        $u_id = $data['users_id'];

        //'comment','votes','spam','reply_id','product_id','user_id'
       $comment->product_id = $p_id;
        $comment->comment = $com;
        $comment->user_id = $u_id;
        $comment->save();

        if($comment)
            return [ "status" => "true","commentId" => $comment->id ];
    }

    public function makeOrder(Request $request) {
        $order = new Order();

        $order->customer_id = $request['customer_id'];
        $order->product_id = $request['product_id'];
        $order->value = $request['value'];
        $order->message = $request['message'];
        $order->pick_date = $request['daytime'];

        $order->save();
        $message = 'Objednávka prebehla úspečne';
        return response()->json(['success' => 'true', 'message' => $message]);
    }

}
