<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DataController extends Controller
{
    public function getCategories() {
        $data = DB::table('categories')->whereNull('parent_id')->get();
        return response($data);
    }
    public function getSubCategories() {
        $categories = DB::table('categories')->whereNull('parent_id')->get();
        $data = [];
        foreach ($categories as $parent) {
            $sub = DB::table('categories')->where('parent_id','=',$parent->id)->get();
            array_push($subCategories,$sub);
        }
        return response($data);
    }
}
