<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;
use App\Comment;
use App\User;
use Auth;
use DB;
use App\CommentVote;
use App\CommentSpam;
use Illuminate\Http\Response;

class CommentController extends Controller
{
    public function index($pageId)

    {

        $comments = Comment::where('product_id',$pageId)->get();

        $commentsData = [];

       foreach ($comments as $key) {
    $a = $key->user_id;
           $user = User::find($key->user_id);

           $name = $user->name;

           $replies = $this->replies($key->id);

           $photo = $user->first()->photo_url;

           // dd($photo->photo_url);

           $reply = 0;

           $vote = 0;

           $voteStatus = 0;

           $spam = 0;

           if(Auth::user()){

               $voteByUser = CommentVote::where('comment_id',$key->id)->where('user_id',Auth::user()->id)->first();

               $spamComment = CommentSpam::where('comment_id',$key->id)->where('user_id',Auth::user()->id)->first();

               if($voteByUser){

                   $vote = 1;

                   $voteStatus = $voteByUser->vote;

               }

               if($spamComment){

                   $spam = 1;

               }

           }

           if(sizeof($replies) > 0){

               $reply = 1;

           }

           if(!$spam){

               array_push($commentsData,[

                   "name" => $name,

                   "photo_url" => (string)$photo,

                   "commentid" => $key->id,

                   "comment" => $key->comment,

                   "votes" => $key->votes,

                   "reply" => $reply,

                   "votedByUser" =>$vote,

                   "vote" =>$voteStatus,

                   "spam" => $spam,

                   "replies" => $replies,

                   "date" => $key->created_at->toDateTimeString()

               ]);

           }

       }

       $collection = collect($commentsData);

       return $collection->sortBy('votes');

   }

    protected function replies($commentId)
    {

        $comments = Comment::where('reply_id', $commentId)->get();

        $replies = [];

        foreach ($comments as $key) {

            $user = User::find($key->users_id);

            $name = $user->name;

            $photo = $user->first()->photo_url;

            $vote = 0;

            $voteStatus = 0;

            $spam = 0;

            if (Auth::user()) {

                $voteByUser = CommentVote::where('comment_id', $key->id)->where('user_id', Auth::user()->id)->first();

                $spamComment = CommentSpam::where('comment_id', $key->id)->where('user_id', Auth::user()->id)->first();

                if ($voteByUser) {

                    $vote = 1;

                    $voteStatus = $voteByUser->vote;

                }

                if ($spamComment) {

                    $spam = 1;

                }

            }

            if (!$spam) {

                array_push($replies, [

                    "name" => $name,

                    "photo_url" => $photo,

                    "commentid" => $key->id,

                    "comment" => $key->comment,

                    "votes" => $key->votes,

                    "votedByUser" => $vote,

                    "vote" => $voteStatus,

                    "spam" => $spam,

                    "date" => $key->created_at->toDateTimeString()

                ]);

            }

            $collection = collect($replies);

            return $collection->sortBy('votes');

        }
    }

    public function storeRate(Request $request) {
        $inputs = $request->all();
        $user = $inputs['user_id'];
        $product = $inputs['product_id'];
        $rate = $inputs['rating'];
        $ratings = DB::table('ratings')->get();
        $rated = DB::table('ratings')->where([
            'user_id' => $user,
            'product_id' => $product
        ])->get();

        if (!$rated->isEmpty()) {
            $rating = $rated;
            $rating->user_id = $user;
            $rating->product_id = $product;
            $rating->rate = $rate;
            $rating->save();
        } else {
            $rating = new Rating();
            $rating->user_id = $user;
            $rating->product_id = $product;
            $rating->rate = $rate;
            $rating->save();
        }

        return response()->json(['success' => 'true']);
    }

    public function getRate(Request $request) {
        $product = $request['product_id'];
        $rated = DB::table('ratings')->where([
            'product_id' => $product
        ])->get();
        $count = 0;
        $rating = 0;
        foreach ($rated as $rate) {
            $rating += $rate->rate;
            $count++;
        }
        $rating = $rating/$count;
        if ($rating == 0) {
            $rating = 3;
        }
        return response()->json(['rating' => $rating]);
    }

}
