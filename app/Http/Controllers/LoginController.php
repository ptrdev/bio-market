<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class LoginController extends Controller
{
    public function login(Request $request) {

        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]))
        {
            $user = User::where('email', $request->email)->first();

            if($user->hasRole('admin')) {
                return redirect()->route('cpanel');
            }

            /*if($user->hasRole('seller')) {
                return redirect()->back();
            }*/
            $products = DB::table('products')
                ->select('products.*', 'villages.fullname AS village')
                ->take(8)->orderBy('products.created_at','DESC')
                ->join('users', 'users.id', '=', 'products.user_id')
                ->leftjoin('villages', 'villages.id', '=', 'users.village_id')
                ->get();
            $favourite = DB::table('products')
                ->select('products.*', 'ratings.rate', 'villages.fullname AS village')
                ->join('ratings', 'ratings.product_id', '=', 'products.id')
                ->join('users', 'users.id', '=', 'products.user_id')
                ->leftjoin('villages', 'villages.id', '=', 'users.village_id')
                ->take(8)->orderBy('rate','DESC')->get();
            $categories = DB::table('categories')->whereNull('parent_id')->get();
            $subCategories = [];
            foreach ($categories as $parent) {
                $sub = DB::table('categories')->where('parent_id','=',$parent->id)->get();
                array_push($subCategories,$sub);
            }
            //return redirect()->back();
            return redirect()->route('home',compact('products','categories','subCategories', 'favourite'));
        }

        return redirect()->back();
    }


}
