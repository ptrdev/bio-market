<?php

namespace App\Http\Controllers\Cpanel;

use DB;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user = User::find(Auth::id());
        if ($user->roles[0]->id === 3) {
            return view('cpanel.admin');
        } else {
            abort(404);
        }

    }

    public function runNode()
    {
        exec('.\ws.sh');
        return response('node is running');
    }

    public function addCategory()
    {
        $categories = Category::all();
        return view('cpanel.add_category',compact('categories'));
    }

    public function createCategory(Request $request)
    {
        $category = new Category();

        $category->name = Input::get('name');
        $category->description = Input::get('desc');
        if (Input::get('category') == null) {
            $category->parent_id = null;
        } else {
            $category->parent_id = Input::get('category');
        }
        $category->id_attr = Input::get('id_attr');
        $category->color_attr = Input::get('color_attr');
        $fileName = null;
        if ($request->file('image')) {
            $file = $request->file('image');
            $fileName = time() . "." . $file->getClientOriginalExtension();
            $file->move('uploads/categories/', $fileName);
        }
        $category->image = 'uploads/categories/'.$fileName;

        $category->save();
        return view('cpanel.admin');
    }

    public function updateCategory(Request $request, $id)
    {
        $category = Category::find($id);

        $category->name = Input::get('name',false);
        $category->description = Input::get('desc',false);
        if (Input::get('category') == null) {
            $category->parent_id = null;
        } else {
            $category->parent_id = Input::get('category',false);
        }
        $category->id_attr = Input::get('id_attr',false);
        $category->color_attr = Input::get('color_attr',false);
        $fileName = null;
        if ($request['image']) {
            $file = $request['image'];
            $category->image = '../'.$file;
        }

        $category->save();

        return view('cpanel.admin');
    }

    public function deleteCategory( $id) {
        $category = Category::find($id);

        $category->delete();

        return view('cpanel.admin');
    }

    public function editCategory($id)
    {
        $category = Category::find($id);

        $categories = Category::all();

        return view('cpanel.edit_category',compact('category','categories'));
    }

    public function categories() {
        $categories = DB::table('categories')->whereNull('parent_id')->get();
        $subCategories = [];
        foreach ($categories as $parent) {
            $sub = DB::table('categories')->where('parent_id','=',$parent->id)->get();
            array_push($subCategories,$sub);
        }
        return view('cpanel.categories',compact('categories','subCategories'));
    }


}
