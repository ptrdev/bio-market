<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public function image()
    {
        return $this->hasMany(Image::class);
    }

    protected $table = 'gallery';

}
