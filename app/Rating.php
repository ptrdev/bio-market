<?php
/**
 * Created by PhpStorm.
 * User: PT
 * Date: 1/3/2018
 * Time: 11:25 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}