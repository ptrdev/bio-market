<?php
use App\Product;
use App\User;
use App\Category;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// shared paths
view()->share('public',url('public').'/');

/*
 * pages routes
 */
Route::get('/', 'HomeController@index')->name('home');

Route::get('/detail/{id}', [
    'uses' => 'PagesController@detail',
    'as' => 'product.detail'
]);

Route::get('/near/{latitude}/{longitude}', 'PagesController@near');

Route::get('/newest', [
    'uses' => 'PagesController@newest',
    'as' => 'newest'
]);

Route::get('/category/{id}', [
    'uses' => 'PagesController@category',
    'as' => 'product.category'
]);

Route::get('/search',function(Request $request){
    if (Input::get('query')) {
        $query = Input::get('query');
        $id = Input::get('cat');
        $children = '';
        $category = Category::find($id);
        if ($category != 0) {
            if (!$category->parent_id) {
                $children = $category->children;
            }
            if (!$category->parent_id) {
                $arr = [];
                foreach ($children as $child) {
                    array_push($arr, $child->id);
                }
                array_push($arr, $category);
                $products = Product::where('name','like','%'.$query.'%')->whereIn('category_id', $arr)->limit(4)->get();
            } else {
                $products = Product::where('name','like','%'.$query.'%')->where('category_id','=',$category)->limit(4)->get();
            }
        } else {
            $products = Product::where('name','like','%'.$query.'%')->limit(4)->get();
        }
        return response()->json($products);
    }

});

Route::get('/message/{id}', [
    'uses' => 'MessagesController@sendMessage',
    'as' => 'send.message'
]);

Route::post('messages', 'ChatsController@sendMessage');

view()->share('cpanel',url('public/cpanel').'/');



Route::get('/detail/comments/{id}', 'CommentController@index');
Route::post('/rate', 'CommentController@storeRate');
Route::get('/getRate', 'CommentController@getRate');
Route::post('/detail/{id}/comments', 'PagesController@storeComment');
/*
Route::post('comments/{commentId}/{type}', 'CommentController@update');
*/
Route::get('/seller-detail/{id}', [
    'uses' => 'PagesController@sellerDetail',
    'as' => 'seller.detail'
]);

// auth routes

Auth::routes();
Route::get('/register', function () {
    $categories = DB::table('categories')->whereNull('parent_id')->get();
    $subCategories = [];

    foreach ($categories as $parent) {
        $sub = DB::table('categories')->where('parent_id','=',$parent->id)->get();
        array_push($subCategories,$sub);
    }
    return view('auth.register', compact('categories','subCategories'));
})->name('register');

Route::post('/registerUser', [
    'uses' => 'Auth\RegisterController@create',
    'as' => 'registerUser'
]);

Route::post('/login/custom', [
    'uses' => 'LoginController@login',
    'as' => 'login.custom'
]);


Route::group([ 'middleware' => 'auth'], function (){
Route::get('/cpanel', 'Cpanel\HomeController@index')->name('cpanel');
Route::get('/dashboard', 'Seller\HomeController@index')->name('dashboard');

});

//user
Route::get('dashboard/add_product/{id}','Seller\HomeController@addProduct');
Route::post('dashboard/storeProduct/{id}', 'Seller\HomeController@createProduct');
Route::post('/fileUpload/{id}', 'Seller\HomeController@fileUpload');
//Route::patch('/dashboard/update/{id}', array ('as' => 'update', 'uses' => 'Seller\HomeController@update'));
Route::patch('/dashboard/update_order_status/{id}/{status}', 'Seller\HomeController@updateOrderStatus');
Route::patch('/dashboard/updateProduct/{id}', array ('as' => 'update', 'uses' => 'Seller\HomeController@updateProduct'));
//Route::get('/dashboard/edit/{id}', array ('as' => 'edit', 'uses' => 'Seller\HomeController@edit'));
Route::get('/dashboard/editProduct/{id}', array ('as' => 'edit', 'uses' => 'Seller\HomeController@editProduct'));
Route::get('/dashboard/delete/{id}', array ('as' => 'delete', 'uses' => 'Seller\HomeController@delete'));
Route::get('/dashboard/orders', 'Seller\HomeController@myOrders');
Route::get('/dashboard/myOrders', 'Seller\HomeController@orders');
Route::get('/dashboard/products', 'Seller\HomeController@productsAll');
Route::patch('/dashboard/update_user/{id}', array ('as' => 'update_user', 'uses' => 'Seller\HomeController@updateUser'));
Route::get('/dashboard/update_user_info/{id}', 'Seller\HomeController@updateUserInfo');
Route::get('/dashboard/gallery/{id}', 'GalleryController@index');

//cpanel
Route::get('cpanel/add_category','Cpanel\HomeController@addCategory');
Route::post('cpanel/storeCategory','Cpanel\HomeController@createCategory');
Route::get('cpanel/run','Cpanel\HomeController@runNode');
Route::get('cpanel/categories','Cpanel\HomeController@categories');
Route::get('/cpanel/edit/{id}', array ('as' => 'editCategory', 'uses' => 'Cpanel\HomeController@editCategory'));
Route::patch('/cpanel/update_category/{id}', array ('as' => 'update_category', 'uses' => 'Cpanel\HomeController@updateCategory'));
Route::get('/cpanel/deleteCategory/{id}', 'Cpanel\HomeController@deleteCategory');

//customer
Route::post('/makeOrder','PagesController@makeOrder');

//data
Route::get('/get_categories','DataController@getCategories');
Route::get('/get_sub_categories','DataController@getSubCategories');

//messages
Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});