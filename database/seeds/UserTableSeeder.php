<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_customer = Role::where('name', 'customer')->first();
        $role_seller  = Role::where('name', 'seller')->first();
        $role_admin  = Role::where('name', 'admin')->first();

        $customer = new User();
        $customer->name = 'Zakaznik';
        $customer->email = 'customer@localhost.com';
        $customer->password = bcrypt('secret');
        $customer->save();
        $customer->roles()->attach($role_customer);

        $seller = new User();
        $seller->name = 'Predajca';
        $seller->email = 'seller@localhost.com';
        $seller->password = bcrypt('secret');
        $seller->save();
        $seller->roles()->attach($role_seller);


        $customer = new User();
        $customer->name = 'Admin';
        $customer->email = 'admin@localhost.com';
        $customer->password = bcrypt('secret');
        $customer->save();
        $customer->roles()->attach($role_admin);

    }
}
