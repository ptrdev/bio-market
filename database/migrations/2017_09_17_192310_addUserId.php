<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class addUserId extends Migration {

    public function up()
    {
        Schema::table('products', function($table)
        {
            $table->integer('user_id');
        });

    }

    public function down()
    {
        Schema::table('products', function($table)
        {
            $table->dropColumn('user_id');
        });
    }

}
