<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvailability extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function($table)
        {
            $table->dateTime('daily_availability_from');
            $table->dateTime('daily_availability_to');
            $table->dateTime('availability_from');
            $table->dateTime('availability_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function($table)
        {
            $table->dropColumn('daily_availability_from');
            $table->dropColumn('daily_availability_to');
            $table->dropColumn('availability_from');
            $table->dropColumn('availability_to');
        });
    }
}
