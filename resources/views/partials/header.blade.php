<div class="header-top" xmlns:v-on="http://www.w3.org/1999/xhtml">
    <div class="container">
        <div class="wrap">
            <ul>
                @if (Auth::check())
                    @include('app_components.logged_menu')
                @else
                    @include('app_components.signup_menu')
                @endif
            </ul>
            @if (Auth::check() && Auth::user()->hasRole('customer'))
                @include('app_components.customer_menu')
            @endif
            @if (Auth::check() && Auth::user()->hasRole('seller'))
                @include('app_components.seller_menu')
            @endif
        </div>
    </div>
</div>

<div class="header-bottom">
    <div class="container">
        <div class="logo">
            <a href="/">
                <img src="{{ $public }}images/fm-logo.png" alt="">
            </a>
        </div>
        <div class="search-bar">
            <form action="/">
                <div id="search" class="panel-body">
                    <autocomplete></autocomplete>
                </div>


            </form>

        </div>

        <div class="navbar" style="display: none;">
            <ul>
                <li class="vege"><a href="#">Ovocie / Zelenina</a>
                </li>
                <li><a href="#">Pekárenske výrobky</a></li>
                <li><a href="#">Mäsové výrobky</a></li>
                <li><a href="#">Syry</a></li>
                <li><a href="#">Víno</a></li>
                <li><a href="#">Iné</a></li>
            </ul>
        </div>
        <div class="hammenu">
            <div id="menu-btn"><span>Menu</span></div>
        </div>

    </div>



</div>

<div class="nav">
    <div class="container">
        <div class="wrap">

            <ul>
                <li><a onclick="nearestPlaces()" class="clickable">V okolí</a></li>
                <li><a href="{{ route('newest') }}">Najnovšie</a></li>
            </ul>
            @include('shop_components.categories')
        </div>
    </div>
</div>