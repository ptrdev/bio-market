<div id="loginModal" class="modal log">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>

        <div class="box">
            <h3>Prihlásenie</h3>
            <form method="POST" action="{{ route('login.custom') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input name="email" class="form-control" type="email" placeholder="Email" value="{{ old('email') }}" >
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input name="password" class="form-control" type="password" placeholder="Heslo">
                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
                </div>
                <button type="submit" name="submit">Prihlásiť</button>
                <a href="#">Registrovať</a>
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>
            </form>
        </div>

    </div>

</div>
<div id="menuModal" class="side-menu">

    <div class="header">
        <a href="#" id="login2">Prihlásenie</a>
        <a href="#">Registrovať</a>
    </div>
    <ul>

        <li><a href="#">Ovocie / Zelenina</a>

        </li>
        <li><a href="#">Pekárenske výrobky</a></li>
        <li><a href="#">Mäsové výrobky</a></li>
        <li><a href="#">Syry</a></li>
        <li><a href="#">Víno</a></li>
        <li><a href="#">Iné</a></li>

    </ul>

</div>

<div id="orderModal" class="modal log">

    <!-- Modal content -->
    <div class="modal-content order">
        <span class="close">&times;</span>

        <div class="box">
            <h3>Objednávka</h3>
            <form method="POST" id="postOrder">
                <label for="value">Množstvo</label>
                <input type="number" class="form-control" id="value" name="value" value="1" min="1" max="50" >
                <label for="daytime">Čas prevzatia</label>
                <input type="datetime-local" class="form-control" id="datePicker" value="{{ date('d-m-Y\TH:i') }}" name="daytime"">
                <label for="message">Správa</label>
                <input type="text" class="form-control" id="message" name="message"  >
                {{ csrf_field() }}
                <button type="submit">Odoslat</button>
            </form>
        </div>

    </div>

</div>

<div id="messageModal" class="modal log">

    <!-- Modal content -->
    <div class="modal-content order">
        <span class="close">&times;</span>

        <div class="box">
            <h3>Správa</h3>
            <form >
                {{ csrf_field() }}
                <div class="col-md-6">
                    <!-- Subject Form Input -->
                    <div class="form-group">
                        <label class="control-label">Subject</label>
                        <input type="text" class="form-control" name="subject" placeholder="Subject"
                               value="{{ old('subject') }}">
                    </div>

                    <!-- Message Form Input -->
                    <div class="form-group">
                        <label class="control-label">Message</label>
                        <textarea name="message" class="form-control">{{ old('message') }}</textarea>
                    </div>

                <!-- Submit Form Input -->
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary form-control">Submit</button>
                    </div>
                </div>
            </form>
            <form method="POST" id="postMessage">
                <label for="value">Predmet</label>
                <input type="text" class="form-control" id="subject" name="subject">
                <label for="message">Správa</label>
                <textarea type="text" class="form-control" id="msg" name="msg"  >
                </textarea>
                {{ csrf_field() }}
                <button type="submit">Odoslat</button>
            </form>
        </div>

    </div>

</div>