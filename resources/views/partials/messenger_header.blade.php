<div class="header-top" xmlns:v-on="http://www.w3.org/1999/xhtml">
    <div class="container">
        <div class="wrap">
            <ul>
                @if (Auth::check())
                    @include('app_components.logged_menu')
                @else
                    @include('app_components.signup_menu')
                @endif
            </ul>
            @if (Auth::check() && Auth::user()->hasRole('customer'))
                @include('app_components.customer_menu')
            @endif
            @if (Auth::check() && Auth::user()->hasRole('seller'))
                @include('app_components.seller_menu')
            @endif
        </div>
    </div>
</div>

<div class="header-bottom">
    <div class="container">
        <div class="logo">
            <a href="/">
                <img src="{{ $public }}images/fm-logo.png" alt="">
            </a>
        </div>
        <div class="search-bar">


        </div>

        <div class="hammenu">
            <div id="menu-btn"><span>Menu</span></div>
        </div>

    </div>



</div>

