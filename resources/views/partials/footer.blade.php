<div class="footer-fruit">
    <img src="{{ $public }}images/grape.png" alt="">
</div>
<div class="footer-top">

    <div class="container">
        <div class="wrap">

            <div class="col">
                <div class="title">
                    Pre Zákazníkov
                </div>
                <ul class="footer-links">
                    <li><a href="#">Ako to funguje</a></li>
                    <li><a href="#">Cookies</a></li>
                    <li><a href="#">Služby</a></li>
                    <li><a href="#">Obchodné podmienky</a></li>
                </ul>
            </div>
            <div class="col">
                <div class="title">
                    Pre Obchodníkov
                </div>
                <ul class="footer-links">
                    <li><a href="#">Ako to funguje</a></li>
                    <li><a href="#">Cookies</a></li>
                    <li><a href="#">Online podpora</a></li>
                </ul>
            </div>
        </div>

    </div>

</div>

