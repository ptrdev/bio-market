<div class="item">
    <div class="product-item-container">
        @if($product->daily_availability_from != '00:00:00' && $product->daily_availability_to != '00:00:00')
            <div class="ribbon-wrapper">
                <div class="ribbon">Dostupnost denne</div>
            </div>
        @endif
        <a href="{{ route('product.detail', ['id' => $product->id]) }}">
            <figure>
                <img src="{{ $public }}{{ $product->image }}" alt=""></figure>
            <div class="content">
                <div class="title">{{ $product->name }}</div>
                @if($product->village)
                <div class="local"><span class="icon-location2"></span>{{ $product->village }}</div>
                @else
                    <div class="local"><span class="icon-location2"></span>Bratislava</div>
                @endif
                <div class="price">{{ $product->price }} € / {{ $product->unit }}</div>

                <div class="btn">Zistit viac<span class="icon-arrow-right"></span></div>
            </div>
        </a>

    </div>
</div>