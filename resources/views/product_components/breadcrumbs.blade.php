<ul>
    <li><a href="/">Domov</a></li>
    @if ($parent)
        <li><a href="{{ route('product.category', ['id' => $parent->id]) }}">{{ $parent->name }}</a></li>
    @endif
    @if ($cats)
        <li><a href="{{ route('product.category', ['id' => $cats->id]) }}">{{ $cats->name }}</a></li>
    @endif
    <li>{{ $product->name }}</li>

</ul>