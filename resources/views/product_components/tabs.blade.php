<div id="root" class="container">
    <tabs>
        <tab name="Informácie" :selected="true">
            {!!html_entity_decode($product->description )!!}
        </tab>
        <tab name="Komentáre ({{ $comments->count() }})">
            <div id="Comments">
                <div id="comment" class="panel-body">
                    <comment></comment>
                </div>
            </div>
        </tab>
    </tabs>
</div>