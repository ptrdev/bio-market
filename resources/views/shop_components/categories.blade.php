<ul>
    @php
    {{ $index = 0;  }}
    @endphp
    @foreach($categories as $category)
            <li class="{{ $category->id_attr }}"><a href="{{ route('product.category', ['id' => $category->id]) }}">{{ $category->name }}</a>
            <div id="{{ $category->id_attr }}" class="sub-menu {{ $category->id_attr }}" >

            @if(sizeof($subCategories[$index]) != 0)
            <div class="column">
                <ul>
                    @php
                        {{ $i = 0; }}
                    @endphp
                    @foreach($subCategories[$index] as $sub)
                    <li value="{{ $i }}"><a href="{{ route('product.category', ['id' => $sub->id]) }}">{{ $sub->name }}</a></li>
                        @php
                            {{ $i++; }}
                        @endphp
                    @endforeach
                </ul>
            </div>
            <div class="column">-
                <figure>
                    @foreach($subCategories[$index] as $sub)
                    <img class="product-image" src="{{ $public }}{{ $sub->image }}" alt="">
                    @endforeach
                </figure>
            </div>
            @endif
        </div>
        </li>

        @php
        {{ $index++;  }}
        @endphp
    @endforeach
</ul>