@extends('layouts.messenger')

@section('title', "Message")

@section('content')
    <div class="main">
        <div class="container">
    <section class="message">


    <h1>Create a new message</h1>
    <form action="{{ route('messages.store') }}" method="post">
        {{ csrf_field() }}
            <!-- Subject Form Input -->
            <div class="form-group">
                Správa :
                <label title="{{ $user->name }}">
                    <input type="hidden" name="recipients[]" value="{{ $user->id }}">
                    {!!$user->name!!}</label>
            </div>

            <div class="form-group">
                <label class="control-label">Subject</label>
                <input type="text" class="form-control" name="subject" placeholder="Subject"
                       value="{{ old('subject') }}">
            </div>

            <!-- Message Form Input -->
            <div class="form-group">
                <label class="control-label">Message</label>
                <textarea class="form-control my-editor" rows="10" id="message" name="message" ></textarea>
            </div>
@php /*
            @if($users->count() > 0)
                <div class="checkbox">
                    @foreach($users as $user)
                        <label title="{{ $user->name }}"><input type="checkbox" name="recipients[]"
                                                                value="{{ $user->id }}">{!!$user->name!!}</label>
                    @endforeach
                </div>
        @endif
*/ @endphp
        <!-- Submit Form Input -->
            <div class="form-group">
                <button type="submit" class="btn-success">Submit</button>
            </div>

    </form>

    </section>
        </div>
    </div>
@stop