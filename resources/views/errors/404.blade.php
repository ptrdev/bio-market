<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <title>@yield('title') | Bio Market</title>
    @include('partials.assets')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <?php /*
    <script type="text/javascript" src="{!! asset('node_modules/vue/dist/vue.js') !!}"></script>
   */ ?>
</head>
<body>


<main style="margin-top: 0">
    <div class="main">

        <h3>Stránka ktorú hľadáte bohužiaľ neexistuje.</h3>
    </div>
</main>


<footer>

    @include('partials.footer')
</footer>

</body>
@include('partials.scripts')

</html>