@extends('layouts.dash')
@php
    $title = 'Edituj';
@endphp
@section('title', $title)
@section('content')
    <section class="content" style="background-color: white; padding-bottom: 40%; ">
        <div style="max-width: 50vw; margin: 0 auto;">
            {!! Form::model($category, ['method' => 'PATCH','route' => ['update_category', $category->id], 'enctype' => 'multipart/form-data']) !!}
            <div class="form-group">
                <label for="name">Nazov</label>
                {!! Form::text('name', null, array('placeholder' => 'name','class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                <label for="desc">Popis</label>
                {!! Form::text('desc', null, array('placeholder' => 'desc','class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                <label for="desc">ID kategorie</label>
                {!! Form::text('id_attr', null, array('placeholder' => 'id_attr','class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                <label for="desc">Farba kategorie</label>
                {!! Form::text('color_attr', null, array('placeholder' => 'color_attr','class' => 'form-control')) !!}
            </div>
            <div class="form-group">

                <label for="image">Image</label>
                <div class="input-group">
                <span class="input-group-btn">
                <a id="lfm" data-input="image" data-preview="holder-banner" class="btn btn-primary">
                <i class="fa fa-picture-o"></i> Choose
                </a>
                </span>
                    <input id="image" class="form-control" type="text" name="image">
                </div>
                <img id="holder" style="margin-top:15px;max-height:100px;">
                @if ($category->image)
                    <img id="holder" src="../{{$category->image}}" style="margin-top:15px;max-height:100px;">
                @else
                    <img id="holder" style="margin-top:15px;max-height:100px;">
                @endif
            </div>
            <div class="form-group">
                <label for="type">Nadradená kategória</label>

                <select class="form-control" id="category" name="category">
                    <option style="color: darkred; font-weight: 800;" value="">...</option>
                    @foreach( $categories as $cat)
                        @if($cat->parent_id == null)
                            <option style="color: darkred; font-weight: 800;" value="{{ $cat->id }}">{{ $cat->name }}</option>
                        @else
                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            {!! Form::close() !!}
        </div>
    </section>

@endsection