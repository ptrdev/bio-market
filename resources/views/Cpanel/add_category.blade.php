@extends('layouts.dash')
@php
    $title = 'Pridaj kategoriu';
@endphp
@section('title', $title)
@section('content')
    <section class="content" style="background-color: white; padding-bottom: 40%">
        <form style="max-width: 50vw; margin: 0 auto;" action="storeCategory" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Nazov</label>
                <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp" >
            </div>
            <div class="form-group">
                <label for="desc">Popis</label>
                <textarea type="text" class="form-control" id="desc" name="desc" aria-describedby="emailHelp" >
                    </textarea>
            </div>
            <div class="form-group">
                <label for="desc">ID kategorie</label>
                <textarea type="text" class="form-control" id="id_attr" name="id_attr" aria-describedby="emailHelp" >
                    </textarea>
            </div>
            <div class="form-group">
                <label for="desc">Farba kategorie</label>
                <textarea type="text" class="form-control" id="color_attr" name="color_attr" aria-describedby="emailHelp" >
                    </textarea>
            </div>
            <div class="uploadImage">
                <label for="image">Image</label>
                {!! Form::file('image', null) !!}
            </div>
            <div class="form-group">
                <label for="type">Nadradená kategória</label>

                <select class="form-control" id="category" name="category">
                    <option label=" -----None----- " value=""></option>
                    @foreach( $categories as $category)
                        @if($category->parent_id == null)
                            <option style="color: darkred; font-weight: 800;" value="{{ $category->id }}">{{ $category->name }}</option>
                        @else
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            {{ csrf_field() }}

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </section>

@endsection