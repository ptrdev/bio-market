@extends('layouts.dash')
@php
    $title = 'Zobraz kategórie';
@endphp
@section('title', $title)
@section('content')
    <div id="categories" class="list">
<ul>
    @php
        {{ $index = 0;  }}
    @endphp
    @foreach($categories as $category)
        <div class="line" id="{{ $category->id }}">
        <li class="parent">{{ $category->name }}
            @if(sizeof($subCategories[$index]) != 0)
            <ul>
                @foreach($subCategories[$index] as $sub)
                <div class="line" id="{{ $sub->id }}"> <li class="child">{{ $sub->name }} </li> <div class="menu"><a href="{{ URL::route('editCategory', $sub->id) }}">edit</a> <div id="{{ $sub->id }}" class="delete delete-category">x</div> </div> </div>
                @endforeach
            </ul>
            @endif

        </li> <div class="menu"><a href="{{ URL::route('editCategory', $category->id) }}">edit</a> <div id="{{ $category->id }}" class="delete delete-category">x</div> </div>
        </div>
        @php
            {{ $index++;  }}
        @endphp
    @endforeach
</ul>
    </div>
@endsection