<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li><a href="{{ url('/home') }}"><i class="fa fa-shopping-cart"></i> Shop</a></li>
            <li><a href="{{ url('/cpanel') }}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{ url('cpanel/add_category') }}"><i class="fa fa-plus-square"></i> Pridaj kategóriu</a></li>
            <li><a href="{{ url('cpanel/categories') }}"><i class="fa fa-square"></i> Všetky kategórie</a></li>
            <li><a href="{{ url('cpanel/run') }}"><i class="fa fa-square"></i> Run node</a></li>
            <li>
                <a href="../mailbox/mailbox.html">
                    <i class="fa fa-envelope"></i> <span>Mailbox</span>
                    <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
                </a>
            </li>



        </ul>
    </section>
    <!-- /.sidebar -->
</aside>