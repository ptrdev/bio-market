@extends('layouts.biomarket')

@section('title', 'Home')

@section('content')

    <div class="banner">
        <div class="container flex">
            <div class="col">
                @include('shop_components.banner')
            </div>
            <div class="col">
                @include('shop_components.aside')
            </div>
        </div>
    </div>
    <div class="product-list">
        <div class="container">
            <h1>
                Najoblubenejsie
            </h1>
            <div class="product-list-component">
                <div class="list">
                    @forelse($favourite as $product)
                       @include('product_components.single_product_box')
                    @empty

                        <p>nothing</p>
                    @endforelse
                </div>
                <a href="{{ route('newest') }}" class="btn">Ďalšie produkty</a>
            </div>
        </div>
    </div>
    <div class="middle-content">
        <div class="background">
            <div class="wrap">
                <div class="farmer"><img src="{{ $public }}images/farmerbanner.png" alt=""></div>
                <div class="text">
                    <h1>BIO postrek</h1>
                    <p>Lorem ipsum aes verta geus vela kal donor</p>
                </div>
            </div>
        </div>
    </div>
    <div class="product-list">
        <div class="container">
            <h1>
                Najnovsie
            </h1>
            <div class="product-list-component">
                <div class="list">
                    @forelse($products as $product)
                        @include('product_components.single_product_box')
                    @empty

                        <p>nothing</p>
                    @endforelse

                </div>
                <a href="{{ route('newest') }}" class="btn">Ďalšie produkty</a>
            </div>
        </div>
    </div>
@stop