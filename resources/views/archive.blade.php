@extends('layouts.biomarket')

@section('title', $title)

@section('content')



    <div class="product-list">
        <div class="container">
            <h1>
                {{ $title }}
            </h1>
            <div class="product-list-component">
                <div class="list">
                    @forelse($products as $product)
                        @include('product_components.single_product_box')
                    @empty

                        <p>nothing</p>
                    @endforelse

                </div>
                {{ $products->links() }}
            </div>


        </div>
    </div>
@stop