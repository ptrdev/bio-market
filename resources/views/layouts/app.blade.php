<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | Bio Market</title>
    @include('partials.assets')
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>

@include('partials.modals')
<header class="show">
    <div class="header-top" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="container">
            <div class="wrap">
                <ul>
                    @if (Auth::check())
                        @include('app_components.logged_menu')
                    @else
                        @include('app_components.signup_menu')
                    @endif
                </ul>
                @if (Auth::check() && Auth::user()->hasRole('customer'))
                    @include('app_components.customer_menu')
                @endif
                @if (Auth::check() && Auth::user()->hasRole('seller'))
                    @include('app_components.seller_menu')
                @endif
            </div>
        </div>
    </div>

    <div class="header-bottom">
        <div class="container">
            <div class="logo">
                <a href="/">
                    <img src="{{ $public }}images/fm-logo.png" alt="">
                </a>
            </div>
            <div class="search-bar">
                <form action="/">
                    <div id="app" class="panel-body">
                        <autocomplete></autocomplete>
                    </div>


                </form>

            </div>

            <div class="navbar" style="display: none;">
                <ul>
                    <li class="vege"><a href="#">Ovocie / Zelenina</a>
                    </li>
                    <li><a href="#">Pekárenske výrobky</a></li>
                    <li><a href="#">Mäsové výrobky</a></li>
                    <li><a href="#">Syry</a></li>
                    <li><a href="#">Víno</a></li>
                    <li><a href="#">Iné</a></li>
                </ul>
            </div>
            <div class="hammenu">
                <div id="menu-btn"><span>Menu</span></div>
            </div>

        </div>



    </div>


</header>

<main style="margin-top: 0">
    @yield('content')
</main>


<footer>

    @include('partials.footer')
</footer>

</body>
@include('partials.scripts')

</html>