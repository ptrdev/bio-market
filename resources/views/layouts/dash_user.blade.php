<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | Bio Market</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ $public }}css/dashboard/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ $public }}css/dashboard/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ $public }}css/dashboard/AdminLTE.min.css">

    <link rel="stylesheet" href="{{ $public }}css/dashboard/biomarket-skin.css">
    <link rel="stylesheet" href="{{ $public }}css/admin.css">
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
</head>

<body class="hold-transition skin-biomarket sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b></span>
            <!-- logo for regular state and mobile devices -->
            <img src="{{ $public }}images/fm-logo.png" alt="" width="60px">
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->



                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                            <span class="hidden-xs">{{ auth()->user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">


                                <p>
                                    {{ auth()->user()->email }}
                                    <small> @if (auth()->user()->admin == true) admin @endif </small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">

                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    {{ Form::open(array('url' => '/logout')) }}
                                    {{ Form::submit(trans('Sign Out'), ['class' => 'btn btn-default btn-flat']) }}
                                    {{ Form::close() }}
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->

                </ul>
            </div>
        </nav>
    </header>
    <div class="flex">
    @include('dashboard.user_sidebar')


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $title }}
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>

        <!-- /.content -->
    </div>
    </div>
    <footer class="main-footer">

    </footer>

    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<div class="loader"></div>
<!-- Scripts -->

<script src="{{ $public }}js/app.js"></script>
<script src="{{ $public }}js/admin.js"></script>
<script src="{{ $public }}js/demo.js"></script>
<script>
    var editor_config = {
        path_absolute : "/",
        selector: "textarea.my-editor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: true,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }
            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Správca súborov',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };
    tinymce.init(editor_config);
</script>
<script src="{{ $public }}vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    var domain = "";
    $('#lfm').filemanager('image', {prefix: domain});
    $('#lfm-banner').filemanager('image', {prefix: domain});
    $('#lfm-product').filemanager('image', {prefix: domain});
</script>
</body>
</html>