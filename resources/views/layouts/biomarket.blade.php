<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | Bio Market</title>
    @include('partials.assets')
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

@include('partials.modals')
<header class="show">

    @include('partials.header')
</header>

<main style="margin-top: 0">
    @yield('content')
</main>


<footer>

    @include('partials.footer')
</footer>

</body>
@include('partials.scripts')

</html>