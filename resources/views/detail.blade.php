@extends('layouts.biomarket')

@section('title', $product->name)

@section('content')
    <div class="main">
        <section class="breadcrumbs">
            <div class="container">
                @include('product_components.breadcrumbs')
            </div>
        </section>

    <section class="product-detail">
        <div class="container">
            <div class="flex">
                <div class="column" id="desktop-product-detail-gallery-container">
                    <div class="gallery-container">
                        <div class="gallery-preview">

                            <img src="{{ $public }}{{ $product->image }}">
                        </div>
                    </div>
                </div>
                <div class="column content-container">

                    <h1>{{ $product->name }}</h1>

                    <div id="mobile-product-detail-gallery-container"></div>
                    @if(Auth::check() && Auth::id() != $seller->id)
                        <a href="{{ route('send.message', ['id' => $seller->id]) }}">
                            <span class="icon-menu icon-bubbles3"></span> Napísať správu
                        </a>
                    @endif
                    <p class="availability-container enabled">
                        Predajca |
                        <span class="availability-available"><strong>
                            <a href="{{ route('seller.detail', ['id' => $seller->id]) }}">{{ $seller->name }}</a></strong></span>
                    </p>
                    @if($product->daily_availability_from != '00:00:00' && $product->daily_availability_to != '00:00:00')
                    <p class="availability-container enabled">
                        Dostupnost denne |

                            <span class="availability"><strong>
                            {{ $product->daily_availability_from}}</strong>
                            </span>
                            -
                            <span class="availability"><strong>
                            {{ $product->daily_availability_to}}</strong>
                            </span>

                    </p>
                    @endif
                    <div class="product-short-description">
                        <div>
                            <p>Popis produktu:</p>
                            <p>{{ $product->short_description }}</p>

                        </div>
                    </div>

                    <div id="ratings">
                        <stars-rate @rating-selected ="setRating"></stars-rate>
                    </div>
                    <div class="price-container-wrapper">
                        <div class="price-container">
                            <strong>
                                <span class="discount-prices"><span class="woocommerce-Price-amount amount">{{ $product->price }} € / {{ $product->unit }}</span></span>
                            </strong>

                        </div>
                        <div class="button-reserve">
                            <div id="order" class="btn-success">Objednať</div>
                        </div>

                    </div>
        </div>
                </div>
            <?php $i = 0;
            foreach ($comments as $comment) {
                $i++;
            }

            ?>

            @include('product_components.tabs')
            </div>
    </section>
        <div class="product-list">
            <div class="container">
                <h1>
                    Dalsie produkty
                </h1>
                <div class="product-list-component">
                    <div class="list">
                        @forelse($products as $product)
                            @include('product_components.single_product_box')
                        @empty

                            <p>Nema dalsie produkty</p>
                        @endforelse

                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="seller_id" value="{{ $seller->id }}">
@stop