<ul class="user-menu">
    <li>
        <a href="{{ route('dashboard') }}">
            <span class="icon-menu icon-home2"> </span>
        </a>
    </li>
    <li>
        <a id="msg-btn" href="{{ route('messages') }}" >
            <?php $count = Auth::user()->newThreadsCount(); ?>
            @if($count > 0)
                <span id="new-message" class="label label-danger">{{ $count }}</span>
            @endif
            <span class="icon-menu icon-bubbles3"></span>
        </a>
        <div class="messageModal">
            <div class="modalHeader">
                <a href="#">Najnovsie (0)</a>
                <a href="#">Nova sprava</a>
            </div>
            <div class="modalBody">
                <ul class="inboxArea">
                    <li>
                        <a href="#">
                            <div class="profileImage">
                                <figure>
                                    <img src="../images/customer-512.png" alt="">
                                    <p>User Name</p>
                                </figure>
                            </div>
                            <div class="content">
                                <p>fbsdfjknd</p>
                            </div>
                            <div class="time">
                                <p>1:50</p>
                                <p>24.5.2017</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="profileImage">
                                <figure>
                                    <img src="../images/customer-512.png" alt="">
                                    <p>User Name</p>
                                </figure>
                            </div>
                            <div class="content">
                                <p>fbsdfjknd</p>
                            </div>
                            <div class="time">
                                <p>1:50</p>
                                <p>24.5.2017</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="profileImage">
                                <figure>
                                    <img src="../images/customer-512.png" alt="">
                                    <p>User Name</p>
                                </figure>
                            </div>
                            <div class="content">
                                <p>fbsdfjknd</p>
                            </div>
                            <div class="time">
                                <p>1:50</p>
                                <p>24.5.2017</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="modalFooter">
                <a href="#">Zobrazit vsetky</a>
            </div>
        </div>
    </li>
    <li>
        <a href="#">
            <span class="icon-menu icon-user"></span>
        </a>
    </li>
</ul>