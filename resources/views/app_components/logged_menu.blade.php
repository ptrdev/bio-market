<li>
    <a href="{{ route('logout') }}"
       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
        Logout
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</li>
<li>
    <a href="#">{{ Auth::user()->name }}</a>
</li>
<li>
    <a href="#">Info</a>
</li>