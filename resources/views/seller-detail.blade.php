@extends('layouts.biomarket')

@section('title', $seller->name)

@section('content')
    <div class="main">
        <section class="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="/">Domov</a></li>
                    <li>{{ $seller->name }}</li>

                </ul>
            </div>
        </section>

        <div class="container">
            <div class="profile-banner">
                <img src="{{ $public }}{{$seller->banner}}" alt="">
            </div>
            <div class="flex">
            <div class="avatar">
                <img src="{{ $public }}{{$seller->avatar}}" alt="" />
            </div>
                <div class="seller-name">
                    <h2>{{ $seller->name }}</h2>
                </div>
            </div>
            <div class="seller-info">
                {!!html_entity_decode($seller->description )!!}
            </div>
        </div>

        <div class="product-list">
            <div class="container">
                <h1>
                    Produkty
                </h1>
                <div class="product-list-component">
                    <div class="list">
                        @forelse($products as $product)
                            <div class="item">
                                <div class="product-item-container">
                                    <a href="{{ route('product.detail', ['id' => $product->id]) }}">
                                        <figure><img src="{{ $public }}{{ $product->image }}" alt=""></figure>
                                        <div class="content">
                                            <div class="title">{{ $product->name }}</div>
                                            <div class="local"><span class="icon-location2"></span>Bratislava</div>
                                            <div class="price">{{ $product->price }} € / {{ $product->unit }}</div>
                                            <div class="btn">Zistit viac<span class="icon-arrow-right"></span></div>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        @empty

                            <p>Nema dalsie produkty</p>
                        @endforelse

                    </div>

                </div>


            </div>
        </div>

    </div>
@stop