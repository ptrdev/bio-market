@extends('layouts.dash_user')
@php
    $title = 'Pridanie produktu';
@endphp
@section('title', $title)
@section('content')

        <form style="max-width: 50vw; margin: 0 auto;" class="add" id="{{ $user->id }}" action="/dashboard/storeProduct/{{ $user->id }}" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Nazov</label>
                <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp" >


            </div>
            <div class="form-group">

                <label for="image">Obrazok produktu</label>
                <div class="input-group">
                <span class="input-group-btn">
                <a id="lfm-product" data-input="image" data-preview="holder" class="btn btn-primary">
                <i class="fa fa-picture-o"></i> Choose
                </a>
                </span>
                    <input id="image" class="form-control" type="text" name="image">
                </div>
                    <img id="holder" style="margin-top:15px;max-height:100px;">
            </div>

            <div class="form-group">
                <label for="short_description">Krátky popis</label>
                <input type="text" class="form-control" id="short_description" name="short_description" >
            </div>
            <div class="form-group">
                <label for="description">Popis</label>
                <textarea class="form-control my-editor" id="description" name="description" ></textarea>
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                {!! Form::number('price', null, array('placeholder' => 'price','class' => 'form-control')) !!}

            </div>
            <div class="form-group">
                <label for="type">Jednotka</label>
                <select class="form-control" id="unit" name="unit">

                    <option value="Kg">Kilogram</option>
                    <option value="Ks">Kus</option>
                    <option value="l">Liter</option>
                    <option value="g">Gram</option>

                </select>
            </div>
            <div class="form-group">
                <label for="type">Kategória produktu</label>
                <select class="form-control" id="category" name="category">
                    @foreach( $categories as $category)
                        @if($category->parent_id == null)
                            <option style="color: darkred; font-weight: 800;" value="{{ $category->id }}">{{ $category->name }}</option>
                        @else
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div id="root" class="container">
                <tabs>
                    <tab name="Dostupnost (Denne)" :selected="true">
                        <div class="form-group">
                            <div class="flex">
                                <div class="col">
                                    <label for="time_from">Od</label>
                                    <input id="time_from" name="time_from" type="time">
                                </div>
                                <div class="col">
                                    <label for="time_to">Do</label>
                                    <input id="time_to" name="time_to" type="time">
                                </div>
                            </div>
                        </div>
                    </tab>
                    <tab name="Dostupnost (Specialne)">
                        <div class="form-group">
                            <div class="flex">
                                <div class="col">
                                    <label for="date_from">Od</label>
                                    <input id="date_from" name="date_from" type="datetime-local">
                                </div>
                                <div class="col">
                                    <label for="date_to">Do</label>
                                    <input id="date_to" name="date_to" type="datetime-local">
                                </div>
                            </div>
                        </div>
                    </tab>
                </tabs>
            </div>



            {{ csrf_field() }}

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

@endsection