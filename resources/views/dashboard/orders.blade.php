@extends('layouts.dash_user')
@php
    $title = 'Obdržané objednávky';
@endphp
@section('title', $title)

@section('content')
    <div class="container">
        <div class="table-responsive">
<div class="divTable blueTable">
    <div class="divTableHeading">
        <div class="divTableRow">
            <div class="divTableHead">ID objednávky</div>
            <div class="divTableHead">Zákazník</div>
            <div class="divTableHead">Email</div>
            <div class="divTableHead">Produkt</div>
            <div class="divTableHead">Množstvo</div>
            <div class="divTableHead">Správa</div>
            <div class="divTableHead">Dátum prevzatia</div>
            <div class="divTableHead">Dátum vytvorenia</div>
            <div class="divTableHead">Stav</div>
        </div>
    </div>
    <div class="divTableBody">
        @forelse($orders as $order)
        <div class="divTableRow">

            <div class="divTableCell">{{ $order->id }}</div>
            <div class="divTableCell">{{ $order->name }}</div>
            <div class="divTableCell">{{ $order->email }}</div>
            <div class="divTableCell">{{ $order->prod_name }}</div>
            <div class="divTableCell">{{ $order->value }} / {{ $order->unit }}</div>
            <div class="divTableCell">{{ $order->message }}</div>
            <div class="divTableCell">{{ $order->pick_date }}</div>
            <div class="divTableCell">{{ $order->created_at }}</div>
            <div class="divTableCell status" id="status_{{ $order->id }}">
                @if($order->status == 2)
                    <div class="success">Schválená</div>
                @elseif($order->status == 1)
                    <div class="error">Zamietnutá</div>
                @else
                    <div class="act-btn accept-order"><i class="fa fa-check success"></i><input type="hidden" value="{{ $order->id }}"></div>
                    <div class="act-btn dismiss-order"><i class="fa fa-close error"></i><input type="hidden" value="{{ $order->id }}"></div>
                @endif
            </div>

        </div>
        @empty
            <p>žiadne objednávky</p>
        @endforelse
    </div>
</div>
<div class="blueTable outerTableFooter">

</div>
        </div>
    </div>
@endsection
