@extends('layouts.dash_user')
@php
    $title = 'Galéria produktu';
@endphp
@section('title', $title)

@section('content')

@if (count($errors) > 0)
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

<form action="/upload" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="form-group">

        <label for="image">Pridaj</label>
        <div class="input-group">
                <span class="input-group-btn">
                <a id="lfm-product" data-input="gallery" data-preview="holder" class="btn btn-primary">
                <i class="fa fa-picture-o"></i> Choose
                </a>
                </span>
            <input id="gallery" class="form-control" type="text" name="gallery[]">
        </div>
        @if ($images)
            @foreach($images as $image)
                <img id="holder" src="../{{$image->image }}" style="margin-top:15px;max-height:100px;">
            @endforeach

        @else
            <img id="holder" style="margin-top:15px;max-height:100px;">
        @endif

    </div>
    <input type="submit" value="Upload" />
</form>

@endsection
