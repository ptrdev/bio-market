<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li><a href="{{ url('/') }}"><i class="fa fa-shopping-cart"></i>Shop</a></li>
            <li><a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i>Home</a></li>
            <li><a href="{{ url('dashboard/add_product/'.$id) }}"><i class="fa fa-plus-square"></i>Pridaj produkt</a></li>
            <li><a href="{{ url('dashboard/products') }}"><i class="fa fa-square"></i> Všetky produkty</a></li>
            <li><a href="{{ url('dashboard/myOrders') }}"><i class="fa fa-send"></i>Moje objednávky</a></li>
            <li><a href="{{ url('dashboard/orders') }}"><i class="fa fa-check-square-o"></i>Obdržané objednávky</a></li>
            <li><a href="{{ url('dashboard/update_user_info/'.$id) }}"><i class="fa fa-cogs"></i>Upraviť profil</a></li>
            <li><a href="{{ url('seller-detail/'.$id) }}"><i class="fa fa-search"></i>Zobraziť môj profil</a></li>
            <li>
                <a id="msg-btn" href="{{ route('messages') }}" >
                    <i class="fa fa-envelope"></i> <span>Inbox</span>
                    <?php $count = Auth::user()->newThreadsCount(); ?>
                    @if($count > 0)
                    <span class="pull-right-container">
                                <small id="new-message" class="label pull-right bg-red">{{ $count }}</small>
                    </span>
                    @endif
                </a>
            </li>



        </ul>
    </section>
    <!-- /.sidebar -->
</aside>