@extends('layouts.dash_user')
@php
    $title = 'Úprava produktu';
@endphp
@section('title', $title)

@section('content')
    <section class="content" style="background-color: white; padding-bottom: 40%; ">
        <div style="max-width: 50vw; margin: 0 auto;">
            {!! Form::model($product, ['method' => 'PATCH','route' => ['update', $product->id]]) !!}
            <div class="form-group">
                <label for="name">Nazov</label>
                {!! Form::text('name', null, array('placeholder' => 'name','class' => 'form-control')) !!}
            </div>

            <div class="form-group">

                <label for="image">Obrazok produktu</label>
                <div class="input-group">
                <span class="input-group-btn">
                <a id="lfm-product" data-input="image" data-preview="holder" class="btn btn-primary">
                <i class="fa fa-picture-o"></i> Choose
                </a>
                </span>
                    <input id="image" class="form-control" type="text" name="image">
                </div>
                @if ($product->image )
                    <img id="holder" src="{{ $public }}{{$product->image }}" style="margin-top:15px;max-height:100px;">
                @else
                    <img id="holder" style="margin-top:15px;max-height:100px;">
                @endif

            </div>
            <div class="form-group">
                <a href="{{ url('dashboard/gallery/'.$product->id) }}" class="btn btn-success">Galeria</a>
            </div>
            <div class="form-group">
                <label for="description">Krátky popis</label>
                {!! Form::text('short_description', null, array('placeholder' => 'short_description','class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                <label for="description">Popis</label>
                {!! Form::textarea('description', null, array('placeholder' => 'description','class' => 'form-control my-editor')) !!}
            </div>
            <div class="form-group">
                <label for="price">Price</label>
                {!! Form::number('price', null, array('placeholder' => 'price','class' => 'form-control')) !!}

            </div>
            <div class="form-group">
                <label for="discount">Discount</label>
                {!! Form::number('discount', null, array('placeholder' => 'discount','class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                <label for="type">Kategoria</label>
                <select class="form-control" id="type" name="type">
                    @foreach( $categories as $category)
                        @if($category->parent_id == null)
                            <option style="color: darkred; font-weight: 800;" value="{{ $category->id }}">{{ $category->name }}</option>
                        @else
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div id="root" class="container">
                <tabs>
                    <tab name="Dostupnost (Denne)" :selected="true">
                        <div class="form-group">
                            <div class="flex">
                                <div class="col">
                                    <label for="time_from">Od</label>
                                    <input id="time_from" name="time_from" type="time">
                                </div>
                                <div class="col">
                                    <label for="time_to">Do</label>
                                    <input id="time_to" name="time_to" type="time">
                                </div>
                            </div>
                        </div>
                    </tab>
                    <tab name="Dostupnost (Specialne)">
                        <div class="form-group">
                            <div class="flex">
                                <div class="col">
                                    <label for="date_from">Od</label>
                                    <input id="date_from" name="date_from" type="datetime-local">
                                </div>
                                <div class="col">
                                    <label for="date_to">Do</label>
                                    <input id="date_to" name="date_to" type="datetime-local">
                                </div>
                            </div>
                        </div>
                    </tab>
                </tabs>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            {!! Form::close() !!}
        </div>
    </section>

@endsection