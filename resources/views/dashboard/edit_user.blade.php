@extends('layouts.dash_user')
@php
    $title = 'Úprava užívateľských informácií';
@endphp
@section('title', $title)

@section('content')
    <section class="content" style="background-color: white; padding-bottom: 40%; ">
        <div style="max-width: 50vw; margin: 0 auto;">
            {!! Form::model($user, ['method' => 'PATCH','route' => ['update_user', $user->id], 'enctype' => 'multipart/form-data'] ) !!}
            <div class="form-group">
                <label for="name">Meno</label>
                {!! Form::text('name', null, array('placeholder' => 'name','class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                <label for="name">Email</label>
                {!! Form::text('email', null, array('placeholder' => 'email','class' => 'form-control')) !!}
            </div>
            <div class="form-group">

                <label for="avatar">Avatar</label>
                <div class="input-group">
                <span class="input-group-btn">
                <a id="lfm" data-input="avatar" data-preview="holder" class="btn btn-primary">
                <i class="fa fa-picture-o"></i> Choose
                </a>
                </span>
                    <input id="avatar" class="form-control" type="text" name="avatar">
                </div>
                @if ($user->avatar)
                    <img id="holder" src="{{ $public }}{{$user->avatar}}" style="margin-top:15px;max-height:100px;">
                @else
                    <img id="holder" style="margin-top:15px;max-height:100px;">
                @endif

            </div>

            <div class="form-group">

                <label for="banner">Banner</label>
                <div class="input-group">
                <span class="input-group-btn">
                <a id="lfm-banner" data-input="banner" data-preview="holder-banner" class="btn btn-primary">
                <i class="fa fa-picture-o"></i> Choose
                </a>
                </span>
                    <input id="banner" class="form-control" type="text" name="banner">
                </div>
                <img id="holder" style="margin-top:15px;max-height:100px;">
                @if ($user->banner)
                    <img id="holder-banner" src="{{ $public }}{{$user->banner}}" style="margin-top:15px;max-height:100px;">
                @else
                    <img id="holder" style="margin-top:15px;max-height:100px;">
                @endif
            </div>
            <div class="form-group">
                <label for="village">Miesto predaja</label>
                <select class="form-control" id="village" name="village">
                    @foreach( $villages as $village)
                        @if($village)
                            <option value="{{ $village->id }}">{{ $village->fullname }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="description">Popis</label>
                {!! Form::textarea('description', null, array('placeholder' => 'description','class' => 'form-control my-editor')) !!}
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            {!! Form::close() !!}
        </div>
    </section>

@endsection