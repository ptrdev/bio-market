@extends('layouts.dash_user')
@php
    $title = 'Všetky produkty';
@endphp
@section('title', $title)

@section('content')
    <div class="container">
        <div class="table-responsive">

            @if ( !$products->count() )
                nemate ziadne produkty.
            @else
                <ul id="product-table" style="list-style: none; display: inline-block; ">
                    @foreach( $products as $product )
                        <div class="product-list" id="product{{$product->id}}">
                            <div class="product-box">
                                <li><img src="{{ $public }}{{ $product->image }}"></li>

                                <li><p class="name"> {{ $product->name }}</p></li>
                                <div class="menu-tab">
                                    <li><button class="btn btn-flat"><a href="{{ URL::route('edit', $product->id) }}">edit</a></button></li>
                                    <li><div class="delete" id="{{ $product->id }}" >x</div></li>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </ul>


            @endif

        </div>
    </div>
@endsection