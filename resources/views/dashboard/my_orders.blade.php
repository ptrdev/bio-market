@extends('layouts.dash_user')
@php
    $title = 'Moje objednávky';
@endphp
@section('title', $title)

@section('content')
    <div class="container">
        <div class="table-responsive">
            <div class="divTable blueTable">
                <div class="divTableHeading">
                    <div class="divTableRow">
                        <div class="divTableHead">ID objednávky</div>
                        <div class="divTableHead">Predajca</div>
                        <div class="divTableHead">Email</div>
                        <div class="divTableHead">Produkt</div>
                        <div class="divTableHead">Množstvo</div>
                        <div class="divTableHead">Správa</div>
                        <div class="divTableHead">Dátum prevzatia</div>
                        <div class="divTableHead">Dátum vytvorenia</div>
                        <div class="divTableHead">Stav</div>
                    </div>
                </div>
                <div class="divTableBody">
                    @forelse($orders as $order)
                    <div class="divTableRow">

                            <div class="divTableCell">{{ $order->id }}</div>
                            <div class="divTableCell">{{ $order->name }}</div>
                            <div class="divTableCell">{{ $order->email }}</div>
                            <div class="divTableCell">{{ $order->prod_name }}</div>
                            <div class="divTableCell">{{ $order->value }} / {{ $order->unit }}</div>
                            <div class="divTableCell">{{ $order->message }}</div>
                            <div class="divTableCell">{{ $order->pick_date }}</div>
                            <div class="divTableCell">{{ $order->created_at }}</div>
                            @if($order->status == 2)
                            <div class="divTableCell success">Schválená</div>
                            @elseif($order->status == 1)
                            <div class="divTableCell error">Zamietnutá</div>
                            @else
                            <div class="divTableCell unknown">Neznámy</div>
                            @endif
                    </div>
                    @empty
                        <p>žiadne objednávky</p>
                    @endforelse
                </div>
            </div>
            <div class="blueTable outerTableFooter">

            </div>
        </div>
    </div>
@endsection
