@extends('layouts.biomarket')

@section('title', $category->name)

@section('content')

    <div class="product-list">
        <div class="container">
            <h1>
                {{ $category->name }}
            </h1>
            <div class="product-list-component">
                <div class="list">
                    @forelse($products as $product)
                        @include('product_components.single_product_box')
                    @empty
                        <p>nothing</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@stop