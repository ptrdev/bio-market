@extends('layouts.messenger')

@section('title', "Message")

@section('content')
    <div class="main">
        <div class="container">
            <section class="messages-thread">
    @include('messenger.partials.flash')

    @each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')
            </section>
        </div>
    </div>
@stop