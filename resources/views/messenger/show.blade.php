@extends('layouts.messenger')

@section('title', "Message")

@section('content')
    <div class="main">
        <div class="container">
            <section class="messages-thread">


                <h3>{{ $thread->subject }}</h3>
                @each('messenger.partials.messages', $thread->messages, 'message')

                @include('messenger.partials.form-message')
            </section>
        </div>
    </div>
@stop