<?php $class = $thread->isUnread(Auth::id()) ? 'alert-info' : '';
        $string = html_entity_decode($thread->latestMessage->body );
?>
<div class="container">
<div class="media alert {{ $class }}">
    <a href="{{ route('messages.show', $thread->id) }}">
        @if($thread->userUnreadMessagesCount(Auth::id()) > 0)
            <span class="notification unread"></span>
        @endif
    <h4 class="media-heading">
        {{ $thread->subject }}</h4>
    <p>

        {!! str_limit($string, $limit = 120, $end = '...') !!}
    </p>
    </a>
</div>
</div>