
<form action="{{ route('messages.update', $thread->id) }}" method="post">
{{ method_field('put') }}
{{ csrf_field() }}

<!-- Message Form Input -->
    <div class="form-group">
        Komu :
        <label title="{{ $user->name }}">
            <input type="hidden" name="recipients[]" value="{{ $user->id }}">
            {!!$user->name!!}</label>
    </div>
    <div class="form-group">
        <textarea class="form-control my-editor" rows="10" id="message" name="message" >{{ old('message') }}</textarea>
    </div>



<!-- Submit Form Input -->
    <div class="form-group">
        <button type="submit" class="btn-success">Submit</button>
    </div>
</form>