
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
var VueRouter = require('vue-router/dist/vue-router.js');

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/x-www-form-urlencoded'
};


window.Vue = require('vue');
Vue.use(VueRouter);
Vue.config.devtools = true;
Vue.config.debug = false;
Vue.config.silent = false;
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('stars-rate', require('./components/Stars.vue'));
if ($('#ratings').length) {
    const ratings = new Vue({
        el: '#ratings',
        methods: {
            setRating: function(rating) {
                this.rating = "You have Selected: " + rating + " stars";
            },
            fetchRating() {
            }
        }
    });
}

Vue.component('slider', require('./components/Banner.vue'));
if ($('#banner').length) {
    const banner = new Vue({
        el: '#banner',
    });
}

Vue.component('comment', require('./components/Comments.vue'));
if ($('#comments').length) {
    const comment = new Vue({
        el: '#comments'
    });
}


Vue.component('tabs', {
    template: `
        <div>
            <div class="tabs">
              <ul>
                <li v-for="tab in tabs" :class="{ 'is-active': tab.isActive }">
                    <a :href="tab.href" @click="selectTab(tab)">{{ tab.name }}</a>
                </li>
              </ul>
            </div>

            <div class="tabs-details">
                <slot></slot>
            </div>
        </div>
    `,

    data() {
        return {tabs: [] };
    },

    created() {

        this.tabs = this.$children;

    },
    methods: {
        selectTab(selectedTab) {
            this.tabs.forEach(tab => {
                tab.isActive = (tab.name == selectedTab.name);
            });
        }
    }
});

Vue.component('tab', {
    template: `

        <div v-show="isActive"><slot></slot></div>

    `,
    props: {
        name: { required: true },
        selected: { default: false}
    },
    data() {
        return {
            isActive: false
        };
    },
    computed: {
        href() {
            return '#' + this.name.toLowerCase().replace(/ /g, '-');
        }
    },
    mounted() {
        this.isActive = this.selected;
    }
});

if ($('#root').length) {
    new Vue({
        el: '#root'
    });
}
Vue.component('autocomplete', require('./components/Autocomplete.vue'));
if ($('#search').length) {
    const search = new Vue({
        el: '#search'
    });
}







