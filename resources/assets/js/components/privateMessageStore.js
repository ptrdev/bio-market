/**
 * Created by PT on 12/12/2017.
 */

const state = {
    notifications: [],
    messagesRec: [],
    messagesSent: [],
    messages: {
        subject: '',
        message: '',
        sender: '',
    }
}

const mutations = {}

const actions = {}

export default {
    state, mutations, actions
}